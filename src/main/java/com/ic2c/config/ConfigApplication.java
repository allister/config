package com.ic2c.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableDiscoveryClient
@EnableConfigServer
public class ConfigApplication {

    /**
     *  /{name}-{profiles}.yml
     *  /{label}/{name}-{profiles}.yml
     *
     *  name:服务名
     *  profiles：环境
     *
     *  label：分支（branch）
     *
     *
     *  远端git新建以下两个文件 并分别追加env: dev   env: test
     *  order-dev.yml
     *  order-test.yml
     *
     *  访问：http://localhost:8082/order-dev.yml
     *       http://localhost:8082/order-test.yml
     *
     */

    public static void main(String[] args) {
        SpringApplication.run(ConfigApplication.class, args);
    }

}

